import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DataService } from 'src/app/services/data.service';
import { DayModel, EventModel } from 'src/app/models/day.models';

describe('DataService', () => {
  let component: DataService;
  let fixture: ComponentFixture<DataService>;
  let testDataService: DataService;

  const testEventModel: EventModel = {
    id: "202055-0",
    reminder: "Test Reminder",
    city: "Bogotá",
    color: "#3eb7f8",
    startTime: "00:00:01", 
    endTime: "00:00:01",
  }

  const testDayModel : DayModel = {
    id: "202055",
    day: 5,
    month: 5,
    year: 202,
    styles: {
      isWeekend: false,
      dayNotCurrentMonth: false,
      selectedDay: false,
    },
    events: [],
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataService ],
      imports: [ HttpClientTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    testDataService = new DataService();
    // fixture = TestBed.createComponent(DataService);
    // component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(testDataService).toBeTruthy();
  });

  it('should create event', () => {
    expect(Object.assign({}, testDataService.createEvent(testDayModel, testEventModel))).toEqual(
      jasmine.objectContaining(Object.assign({}, testEventModel))
      );
  });
});
