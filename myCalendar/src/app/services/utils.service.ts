import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor() { }

  // Verifys if day is weekend day
  isWeekend(dayName) {
    return ["Sunday", "Saturday"].includes(dayName);
  };

  // Returns date based on year/month
  getDate(year, month) {
    let date = new Date(year, month, 1);
    return date;
  };

  // Returns amount days per month
  getAmountDaysMonth(year, month) {
    return new Date(year, month, 0).getDate();
  };
  
  // Returns previos month
  getMonthBefore(year, month) {
    return this.getDate(year, month).getMonth();
  };

  // Return day as String
  getDayName(date) {
    const options = { weekday: "long" };
    const dayName = new Intl.DateTimeFormat("en-US", options).format(date);
    return dayName;
  };

  // Return the initial day of the month
  initialDay(year, month) {
    const date = this.getDate(year, month);
    date.setDate(1);
    return date.toLocaleString("en-US", { weekday: "long" });
  };

  // Return name of the last day month
  getLastDayMonthName(year, month) {
    const date = this.getDate(year, month);
    date.setDate(this.getAmountDaysMonth(year, month + 1));
    return date.toLocaleString("en-US", { weekday: "long" });
  };

  // Return the position of the first day in the calendar row/week
  positionDayinit(startDay, listDaysName) {
    const index = listDaysName.findIndex((day) => {
        if (startDay === day) {
            return day;
        }
    });
    return index + 1;
  };

  // Return the position of the last day in the calendar row/week
  positionDayFinish(lastDayMonthName, listDaysName) {
    const index = listDaysName.findIndex((day) => {
        if (lastDayMonthName === day) {
            return day;
        }
    });
    return index;
  };

  // Jump from month to month
  changeMonth(year, month, dash){
    // If dash is negative the months are going to go back
    // If month == 12 year will increase/decrease
    if ( month == 12 ) {
        year = year + dash;
        month = 0;
    } else if ( month == 0 && dash == -1 ) {
      year = year + dash;
      month = 11;
    } else {
        month = month + dash;
    }
    return {year, month};
  };

}
