import { Injectable } from '@angular/core';
import { DayModel, EventModel } from '../models/day.models';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  selectedDay: DayModel;

  // Arrays of data main.
  daysEvents: Array<DayModel> = [];
  events: Array<EventModel> = [];

  // Count used to know how many events has been created.
  count: number;

  // Observable.
  private eventsObservable$ = new Subject<EventModel>();

  constructor() {
    this.setInitialDayEvents();
  }

  // Gets data from local storage and sets to dayEvents service var.
  setInitialDayEvents() {
    let objectAllDaysEvents = JSON.parse(localStorage.getItem("DAYS_EVENTS"));
    if (objectAllDaysEvents != undefined) {
      Object.keys(objectAllDaysEvents).forEach(key => {
        const day = objectAllDaysEvents[key];
        this.daysEvents.push(day);
      });
    }
    this.count = Number(localStorage.getItem("COUNT_EVENTS"));
  };

  // Sync vector days in the service and local storage.
  updateLocalStorage() {
    localStorage.setItem("DAYS_EVENTS", JSON.stringify(this.daysEvents));
  };

  // Create a new event with it's corresponding day.
  createEvent(day: DayModel, event: EventModel, useCase?: boolean) {
    let tempEvent = new EventModel(event.id, event.reminder, event.city, event.color, event.startTime, event.endTime)
    let newDay = new DayModel(day.id, day.day, day.month, day.year, day.styles, new Array(tempEvent));
    // Return position index of day.
    let indexDay = this.daysEvents.findIndex((day) => {
      if (day.id == newDay.id) {
        return day
      };
    });
    // If doesn't exist a day add day and its new event, otherwise adds only the event.
    if (indexDay == -1) {
      this.daysEvents.push(newDay);
      this.daysEvents[this.daysEvents.length - 1].events = [];
      this.daysEvents[this.daysEvents.length - 1].events.push(tempEvent);
    } else {
      this.daysEvents[indexDay].events.push(tempEvent);
    }
    // Create new event on observable subscribers.
    this.eventsObservable$.next(tempEvent);

    this.count++;

    // Sets local storage with new event.
    localStorage.setItem("COUNT_EVENTS", this.count.toString());
    localStorage.setItem("DAYS_EVENTS", JSON.stringify(this.daysEvents));
    return tempEvent;
  };

  // Gets events of the day selected.
  getEventsSelectedDay(day: DayModel) {
    let id = day.id;
    // Find the day by id and return.
    let tempSelectedDay = this.daysEvents.find((day) => {
      if (id == day.id) {
        return day;
      };
    });
    // If the day is undefined, it's initialized.
    if (tempSelectedDay == undefined) {
      this.selectedDay = day;
    } else {
      this.selectedDay = tempSelectedDay;
    }
  };

  // Return observable subject for the listeners.
  getObservableEvents$(): Observable<EventModel> {
    return this.eventsObservable$.asObservable();
  };
}
