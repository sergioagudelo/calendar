import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private API_KEY = 'a7b9d7372f517a414b9228aec0c3026a';

  constructor(private http:HttpClient) { }
  
  // Get the weather data by city name
  getWeatherByCity(city:String){
    return this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${this.API_KEY}`);
  }
}
