export class DayModel {
    id: String;
    day: number;
    month: number;
    year: number;
    styles: {
        isWeekend: boolean,
        dayNotCurrentMonth: boolean,
        selectedDay?: boolean,
    };
    events?: EventModel[];
    weather?: {
        name: String,
        feelsLike: number,
        humdity: number,
        temp: number,
        tempMax: number,
        tempMin: number,
        currentWeater: String
    }
    constructor(id = null, day = null, month = null, year = null, styles = null, event = [new EventModel]) {
        this.id = id;
        this.day = day;
        this.month = month;
        this.year = year;
        this.styles = styles;
        this.events = event;
    };
}
export class EventModel {
    id: String;
    reminder: String;
    city: String;
    color: String;
    startTime: String;
    endTime: String;
    constructor(id = null, reminder = null, city = null, color = null, startTime = null, endTime = null) {
        this.id = id;
        this.reminder = reminder;
        this.city = city;
        this.color = color;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}