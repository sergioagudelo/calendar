import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalComponent } from './modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let debugElement: DebugElement;
  let htmlElement: HTMLElement;
  let citys: Array<String> = ['Bogotá', 'Medellín', 'Cali', 'Barranquilla', 'Cúcuta', 'Cartagena', 'Soledad', 'Ibagué']

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalComponent ],
      imports: [ 
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(ModalComponent);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement.query(By.css('form'));
      htmlElement = debugElement.nativeElement;
    });
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(ModalComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have a citys array like ${citys}`, () => {
    expect(component.citys).toEqual(citys);
  });

  it('should call sendEvent method', () => {
    fixture.detectChanges();
    spyOn(component, 'sendEvent');
    htmlElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
    htmlElement.click();
    expect(component.sendEvent).toHaveBeenCalledTimes(0);
  });

  it('form should be invalid', () => {
    expect(component.formEvent.valid).toBeFalsy();
  });

  it('form should be valid', () => {
    component.formEvent.controls['reminder'].setValue('Remidex A');
    expect(component.formEvent.valid).toBeTruthy();
  });

});
