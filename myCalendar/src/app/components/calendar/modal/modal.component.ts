import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

// Models
import { EventModel } from 'src/app/models/day.models';

// Services
import { DataService } from 'src/app/services/data.service';
import { WeatherService } from 'src/app/services/weather.service';
import { DayComponent } from '../day/day.component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @ViewChild(DayComponent) dayComponent: DayComponent;
  // @Output() dayEvent:EventEmitter;
  
  citys: Array<String> = ['Bogotá', 'Medellín', 'Cali', 'Barranquilla', 'Cúcuta', 'Cartagena', 'Soledad', 'Ibagué'];
  cityDefault: String;
  weather: String;
  
  messageModal: String;
  fixModal: boolean = false;

  formEvent: FormGroup;

  event: EventModel;

  // Used to control Edit/Create form use case.
  useCase: boolean;
  idEditedEvent: String;

  constructor(public dataService: DataService, public weatherService: WeatherService) {
    this.createForm('', this.citys[0], '#3eb7f8', '00:00:01', '23:59:50');
    this.weatherService.getWeatherByCity(this.citys[0]).subscribe(resp => {
      this.weather = resp['weather'][0].description;
    });

  }

  ngOnInit(): void {
  }

  sendEvent(form: FormGroup) {
    let id = this.dataService.selectedDay.id;
    if (form.value.startTime < form.value.endTime) {
      this.messageModal = 'Event created/edited successful';
      this.fixModal = false;
      this.event = new EventModel();
      // id based on useCase
      this.event.reminder = form.value.reminder;
      this.event.city = form.value.city;
      this.event.color = form.value.color;
      this.event.startTime = form.value.startTime;
      this.event.endTime = form.value.endTime;
      if ( this.useCase ) {
        this.event.id = this.idEditedEvent;
        this.manegaEvent(this.event);
      } else {
        this.event.id = `${id}-${this.dataService.count}`;        
      }
      this.dataService.createEvent(this.dataService.selectedDay, this.event, this.useCase);
      Swal.fire('Great!', `Event ${this.event.reminder} created!`, 'success')
      // this.formEvent.reset()
    } else {
      this.messageModal = 'Start Time cannot be greater than End Time.';
      this.fixModal = true;
    }
  };

  manegaEvent(event:EventModel){
    let idDay = event.id.split('-')[0];
    let indexDay = this.findIndex(idDay);
    let indexEventOfDay = this.dataService.daysEvents[indexDay].events.findIndex(tempEvent=>{
      if (event.id == tempEvent.id ) {
          return tempEvent;
      };
    });
    this.dataService.daysEvents[indexDay].events.splice(indexEventOfDay,1);
    // this.dataService.daysEvents[indexDay].events.push(event);
    this.dataService.updateLocalStorage();
    window.location.reload();
    
  }

  deleteEvent2(event:EventModel){
    // let idDay = event.id.split('-')[0];
    // let indexDay = this.findIndex(idDay);
    // let indexEventOfDay = this.dataService.daysEvents[indexDay].events.findIndex(tempEvent=>{
    //   if (event.id == tempEvent.id ) {
    //       return tempEvent;
    //   };
    // });
    // this.dataService.daysEvents[indexDay].events.splice(indexEventOfDay,1);
    // this.dataService.updateLocalStorage();
    // indexDay = this.findIndex(idDay);
    // this.selectedDayForModal = this.dataService.daysEvents[indexDay];
    // window.location.reload();
  }

  findIndex(idDay){
    let indexDay = this.dataService.daysEvents.findIndex(day=>{
      if (idDay == day.id) {
        return day;
      }
    });
    return indexDay;
  }

  // Validator for sing up User
  createForm(reminder:String, city:String, color:String, startTime:String, endTime:String) {
    this.formEvent = new FormGroup({
      reminder: new FormControl(reminder, [Validators.required , Validators.maxLength(30)]),
      city: new FormControl(this.citys[0], Validators.required),
      color: new FormControl(color, Validators.required),
      startTime: new FormControl(startTime, Validators.required),
      endTime: new FormControl(endTime, Validators.required),
    });
  };

  // Deletes and Updates events
  manageEvent(useCase: boolean, event: EventModel) {
    this.useCase = useCase;
    if ( useCase ) {
      this.idEditedEvent = event.id; 
      this.createForm(event.reminder, event.city, event.color, event.startTime.toString(), event.endTime.toString())
    } else {
      this.createForm('', this.citys[0], '#3eb7f8', '00:00:01', '23:59:50')
    }
  }


  getWeather(e) {
    this.cityDefault = e.target.value;
    this.weatherService.getWeatherByCity(this.cityDefault).subscribe(resp => {
      this.weather = resp['weather'][0].description;
    });
  }

  // ngAfterViewInit(): void {
  //   //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
  //   //Add 'implements AfterViewInit' to the class.
  //   document.querySelector("#modalDayEvent").addEventListener("click", (event) => {

  //   });
  //   document.querySelector("#modalDayEvent").addEventListener("keydown", (event) => {
  //     if ( event.key == 'Escape' ) {

  //     }
  //   }); 
  // }

}
