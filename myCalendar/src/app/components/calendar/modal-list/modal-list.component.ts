import { Component, OnInit, ViewChild } from '@angular/core';
import { EventModel, DayModel } from 'src/app/models/day.models';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { ModalComponent } from '../modal/modal.component';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-modal-list',
  templateUrl: './modal-list.component.html',
  styleUrls: ['./modal-list.component.css']
})
export class ModalListComponent implements OnInit {
  @ViewChild(ModalComponent) modalComponent: ModalComponent;
  // Observable events from service.
  eventsObservable$: Observable<EventModel>;

  selectedDayForModal: DayModel;
  day: DayModel;

  // Weather array for manage information.
  weathers: Array<String> = [];

  constructor(public dataService: DataService, public weatherService: WeatherService) {
    this.selectedDay(new DayModel());
  }

  ngOnInit(): void {
    this.eventsObservable$ = this.dataService.getObservableEvents$();
  }

  // Receives info from day throug eventEmitter outputSelectedDay.
  selectedDay(day: DayModel) {
    try {
      if (day.events[0].id == null) {
        day.events.shift();
      }
    } catch (error) {
    }
    this.weathers = [];
    this.selectedDayForModal = day;
    this.selectedDayForModal.events.map(async (event) => {
      return await this.getWeather(event.city);
    });
    this.day = day;
  }

  // Gets weather for a specific day.
  getWeather(city) {
    this.weatherService.getWeatherByCity(city).subscribe((res) => {
      this.weathers.push(res['weather'][0].description);
    });
  };

  // Deletes selected event.
  deleteEvent(event: EventModel) {
    let idDay = event.id.split('-')[0];
    let indexDay = this.findIndex(idDay);
    // Return the position event in the day.
    let indexEventOfDay = this.dataService.daysEvents[indexDay].events.findIndex(tempEvent => {
      if (event.id == tempEvent.id) {
        return tempEvent;
      };
    });
    this.dataService.daysEvents[indexDay].events.splice(indexEventOfDay, 1);
    this.dataService.updateLocalStorage();
    indexDay = this.findIndex(idDay);
    this.selectedDayForModal = this.dataService.daysEvents[indexDay];
    window.location.reload();
  }

  // Find index for a day based on his ID.
  findIndex(idDay) {
    let indexDay = this.dataService.daysEvents.findIndex(day => {
      if (idDay == day.id) {
        return day;
      }
    });
    return indexDay;
  }

  // Deletes all event for a selected day.
  deleteAllEvent(day: DayModel) {
    try {
      let indexDay = this.findIndex(day.id);
      this.dataService.daysEvents[indexDay].events = [];
      this.dataService.updateLocalStorage();
      window.location.reload();
    } catch (error) {
    }
  }

  // Create or updates events.
  manageEvent(useCase: boolean, event?: EventModel) {
    this.modalComponent.manageEvent(useCase, event);
  }
}
